from z3 import *
import sys
import argparse
from solverRuns import run_solver, run_dt_solver
from utils.Traces import Trace, ExperimentTraces
from multiprocessing import Process, Queue
import logging

def helper(m, d, vars):
    tt = { k : m[vars[k]] for k in vars if k[0] == d }
    return tt

 
def main():
    
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--traces", dest="tracesFileName", required=True)
    parser.add_argument("--max_size", dest="maxSize", default='8')
    parser.add_argument("--start_size", dest="startSize", default='1')
    parser.add_argument("--max_num_formulas", dest="numFormulas", default='1')
    parser.add_argument("--iteration_step", dest="iterationStep", default='1')
    parser.add_argument("--dt", dest="dt", default=False, action='store_true')
    parser.add_argument("--sat", dest="sat", default=False, action='store_true')
    parser.add_argument("--timeout", dest="timeout", default=600, help="timeout in seconds")
    parser.add_argument("--log", dest="loglevel", default="ERROR")
    parser.add_argument("--universal", dest="universal", action="store_true", default=False, help="whether the learned formula is forced to be of the form G(.)")
    args,unknown = parser.parse_known_args()
    tracesFileName = args.tracesFileName
    
    
    """
    traces is 
     - list of different recorded values (traces)
     - each trace is a list of recordings at time units (time points)
     - each time point is a list of variable values (x1,..., xk) 
    """
    
    numeric_level = args.loglevel.upper()
    logging.basicConfig(level=numeric_level)

    
    maxSize = int(args.maxSize)
    numFormulas = int(args.numFormulas)
    startSize = int(args.startSize)
    traces = ExperimentTraces()
    iterationStep = int(args.iterationStep)
    traces.readTracesFromFile(args.tracesFileName)
    finalSize = int(args.maxSize)
    traces = ExperimentTraces()
    traces.readTracesFromFile(tracesFileName)
    solvingTimeout = int(args.timeout)
    universal = args.universal
    #print(traces)
    timeout = int(args.timeout)
    if args.sat:
        [formulas, timePassed] = run_solver(finalSize=maxSize, traces=traces, maxNumOfFormulas = numFormulas, startValue=startSize, step=iterationStep, universalFormula=universal)
        logging.info("formulas: "+str([f.prettyPrint(f) for f in formulas])+", timePassed: "+str(timePassed)) 
        if len(formulas) == 0:
            print("NO SOLUTION")
        else:
            print(formulas[0])
    elif args.dt:
        if universal:
            logging.error("The --universal option is only valid with the sat algorithm.")
            sys.exit(-1)
        [f, timePassed, numAtoms, numPrimitives] = run_dt_solver(traces=traces)
        logging.info("timePassed: {0}, numAtoms: {1}, numPrimitives: {2}".format(str(timePassed), str(numAtoms), str(numPrimitives)))
        logging.info(f"Formula: {f}")
    else:
        logging.error("Please specify an algorithm to run")        


if __name__ == "__main__":
    main()

